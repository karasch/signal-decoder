import { render, screen } from '@testing-library/react';
import React from 'react';
import renderer from 'react-test-renderer';
import App from './App';
import Light from './App';


test('Div contains red', () => {
  render(<App />);
  const lightElements = screen.getAllByText(/red/i);

  lightElements.forEach((light) => {
    expect(light).toBeInTheDocument();
  });
  //expect(linkElement).toBeInTheDocument();
});





