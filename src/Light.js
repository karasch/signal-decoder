import React, {useState} from 'react';
import * as Constants from './constants';

const Light = ({initialState, colorChange, flashingChange, id}) => {

    const [color, setColor] = useState(initialState);
    const [flashing, setFlashing] = useState(false);

    const onClick = () => {
        if(color === Constants.ASPECTS.RED){
            setColor(Constants.ASPECTS.YELLOW);
            colorChange(Constants.ASPECTS.YELLOW);
        }
        if(color === Constants.ASPECTS.YELLOW){
            setColor(Constants.ASPECTS.GREEN);
            colorChange(Constants.ASPECTS.GREEN);
        }
        if(color === Constants.ASPECTS.GREEN ){
            setColor(Constants.ASPECTS.LUNAR);
            colorChange(Constants.ASPECTS.LUNAR);
        }
        if(color === Constants.ASPECTS.LUNAR){
            setColor(Constants.ASPECTS.RED);
            colorChange(Constants.ASPECTS.RED);
        }
        
    };

    const onChange = () => {
        setFlashing(!flashing);
        flashingChange(!flashing);
    }

    return(
        <div className="lightform">
            <span className="black bigdot">
                <span onClick={onClick} className={`dot ${color}`} data-testid="light-test-id">
                {color}
                </span>
            </span>
            <div>
                <input type="checkbox" id={id} onChange={onChange} checked={flashing} />
                <label htmlFor={id}>Flashing</label>
            </div>
        </div>
    );
};

export default Light;