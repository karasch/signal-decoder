import React from 'react';
import * as Constants from './constants';


const Inidcation = ({topAspect, topFlashing, middleAspect, middleFlashing, bottomAspect, bottomFlashing}) => {

    let indication = "";
    let meaning = "";
    if(topAspect === Constants.ASPECTS.RED && middleAspect === Constants.ASPECTS.RED && bottomAspect === Constants.ASPECTS.RED && 
        !topFlashing && !middleFlashing && !bottomFlashing){
        indication = "Stop";
        meaning = "Train may not pass this signal.";
    } else if (topAspect === Constants.ASPECTS.YELLOW && middleAspect === Constants.ASPECTS.RED && bottomAspect === Constants.ASPECTS.RED &&
        !topFlashing && !middleFlashing && !bottomFlashing){
        indication = "Approach";
        meaning = "Train may pass this signal at track speed prepared to stop before the next signal.";
    } else if (topAspect === Constants.ASPECTS.GREEN && middleAspect === Constants.ASPECTS.RED && bottomAspect === Constants.ASPECTS.RED && 
        !topFlashing && !middleFlashing && !bottomFlashing){
        indication = "Clear";
        meaning = "Train may pass this signal at track speed.";
    } else if ((((topAspect === Constants.ASPECTS.LUNAR && middleAspect === Constants.ASPECTS.RED && bottomAspect === Constants.ASPECTS.RED) ||
                    (topAspect === Constants.ASPECTS.RED && middleAspect === Constants.ASPECTS.LUNAR && bottomAspect === Constants.ASPECTS.RED) ||
                    (topAspect === Constants.ASPECTS.RED && middleAspect === Constants.ASPECTS.RED && bottomAspect === Constants.ASPECTS.LUNAR)) &&
                    !topFlashing && !middleFlashing && !bottomFlashing) ||
                ((topAspect === Constants.ASPECTS.RED && middleAspect === Constants.ASPECTS.RED && bottomAspect === Constants.ASPECTS.RED) &&
                    topFlashing ^ middleFlashing ^ bottomFlashing)){

        indication = "Restricting";
        meaning = "Train may pass this signal as Restricted speed.";
    } else if ((topAspect === Constants.ASPECTS.YELLOW && middleAspect === Constants.ASPECTS.YELLOW && bottomAspect === Constants.ASPECTS.RED &&
        !topFlashing && !middleFlashing && !bottomFlashing) ||
        (topAspect === Constants.ASPECTS.YELLOW && middleAspect === Constants.ASPECTS.RED && bottomAspect === Constants.ASPECTS.RED &&
            topFlashing && !middleFlashing && !bottomFlashing)) {
        indication = "Advance Approach";
        meaning = "Train may pass this signal prepared to stop at the second signal.";
    } else if (topAspect === Constants.ASPECTS.YELLOW && middleAspect === Constants.ASPECTS.GREEN && bottomAspect === Constants.ASPECTS.RED &&
        !topFlashing && middleFlashing && !bottomFlashing) {
            indication = "Approach Limited";
            meaning = "Train may pass this signal at limited speed approaching next signal not exceeding Limited speed.";
    } else if (topAspect === Constants.ASPECTS.RED && middleAspect === Constants.ASPECTS.GREEN && bottomAspect === Constants.ASPECTS.RED &&
        !topFlashing && middleFlashing && !bottomFlashing) {
            indication = "Limited Clear";
            meaning = "Train may pass this signal at Limited speed through turnouts, crossovers, sidings, and over power-operated switches, then proceed.";
    } else if (topAspect === Constants.ASPECTS.YELLOW && middleAspect === Constants.ASPECTS.GREEN && bottomAspect === Constants.ASPECTS.RED &&
                !topFlashing && !middleFlashing && !bottomFlashing) {
        indication = "Approach Medium";
        meaning = "Train may pass this signal approaching next signal not exceeding Medium speed";
    } else if (topAspect === Constants.ASPECTS.RED && middleAspect === Constants.ASPECTS.GREEN && bottomAspect === Constants.ASPECTS.RED &&
        !topFlashing && !middleFlashing && !bottomFlashing) {
        indication = "Medium Clear";
        meaning = "Train may pass this signal at Medium speed through turnouts, crossovers, sidings, and over power-operated switches, then proceed.";
    } else if (topAspect === Constants.ASPECTS.YELLOW && middleAspect === Constants.ASPECTS.RED && bottomAspect === Constants.ASPECTS.GREEN &&
        !topFlashing && !middleFlashing && !bottomFlashing) {
        indication = "Approach Slow";
        meaning = "Train may pass this signal approaching next signal not exceeding Slow speed.";
    } else if (topAspect === Constants.ASPECTS.RED && middleAspect === Constants.ASPECTS.RED && bottomAspect === Constants.ASPECTS.GREEN &&
        !topFlashing && !middleFlashing && !bottomFlashing) {
        indication = "Slow Clear";
        meaning = "Train may pass this signal at Slow speed through turnouts and crossovers, sidings, and over power-operated switches, then proceed.";
    } else if (topAspect === Constants.ASPECTS.YELLOW && middleAspect === Constants.ASPECTS.LUNAR && bottomAspect === Constants.ASPECTS.RED &&
        !topFlashing && !middleFlashing && !bottomFlashing) {
        indication = "Approach Restricting";
        meaning = "Train may pass this signal at restricted speed, prepared to stop at the next signal.";
    } else if (topAspect === Constants.ASPECTS.RED && middleAspect === Constants.ASPECTS.YELLOW && bottomAspect === Constants.ASPECTS.RED &&
        !topFlashing && middleFlashing && !bottomFlashing) {
            indication = "Limited Approach";
            meaning = "Train may pass this signal at Limited speed through turnouts, crossovers, sidings, and over power-operated switches; then proceed, prepared to stop at next signal.";
    } else if (topAspect === Constants.ASPECTS.RED && middleAspect === Constants.ASPECTS.YELLOW && bottomAspect === Constants.ASPECTS.RED && 
        !topFlashing && !middleFlashing && !bottomFlashing) {
        indication = "Medium Approach";
        meaning = "Train may pass this signal at Medium speed through turnouts, crossovers, sidings, and over power-operated switches; then proceed, prepared to stop at next signal.";
    } else if (topAspect === Constants.ASPECTS.RED && middleAspect === Constants.ASPECTS.RED && bottomAspect === Constants.ASPECTS.YELLOW && 
        !topFlashing && !middleFlashing && !bottomFlashing) {
        indication = "Slow Approach";
        meaning = "Train may pass signal at slow speed through turnouts and crossovers, sidings, and over power-operated switches; then proceed, prepared to stop at next signal.";
    } else {
        indication = "No such signal";
        meaning = "There is no indication for this combination of lights.";
    }

//TODO: Medium Approach Medium, Medium Approach Slow, Medium Advance Approach

    return(
        <div>
            <h1 data-testid="indication-name">{indication}</h1>
            <p>{meaning}</p>
        </div>
    );
};

export default Inidcation;