import { render, fireEvent, cleanup } from '@testing-library/react';
import React from 'react';
import { create, renderer } from 'react-test-renderer';
import Light from './Light';
import * as Constants from './constants'

afterEach(cleanup);

/*
// a snapshot test
test('Light changes when clicked', () => {
    const component = renderer.create(
        <Light/>
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    // manually trigger a click / change to yellow
    renderer.act(() =>{
        tree.props.onClick();
    });
    
    //re-render the component
    tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    //manually trigger a click / change to green

    renderer.act( () => {
        tree.props.onClick();
    });

    //re-render the component
    tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    //manually trigger a clock / change back to red
    renderer.act( () => {
        tree.props.onClick();
    });

    //re-render and check
    tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

// a DOM test
test('onClick is set', () => {

    const {container, getByText} = render(<Light />);

    expect(getByText('red')).toBeInTheDocument();

    fireEvent.click(getByText('red'));

    expect(getByText('yellow')).toBeInTheDocument();
});

*/

test('Test light cycles through colors', () => {

    const mockColorChange = jest.fn();
    const container = render(<Light initialState={Constants.ASPECTS.RED} colorChange={mockColorChange} />);

    const light = container.getByTestId("light-test-id");

    expect(light).toHaveClass(Constants.ASPECTS.RED);

    fireEvent.click(light);

    expect(mockColorChange).toHaveBeenCalledWith(Constants.ASPECTS.YELLOW);
    expect(light).toHaveClass(Constants.ASPECTS.YELLOW);

    fireEvent.click(light);

    expect(mockColorChange).toHaveBeenLastCalledWith(Constants.ASPECTS.GREEN);
    expect(light).toHaveClass(Constants.ASPECTS.GREEN);


    fireEvent.click(light);

    expect(mockColorChange).toHaveBeenLastCalledWith(Constants.ASPECTS.LUNAR);
    expect(light).toHaveClass(Constants.ASPECTS.LUNAR);

    fireEvent.click(light);

    expect(mockColorChange).toHaveBeenLastCalledWith(Constants.ASPECTS.RED);
    expect(light).toHaveClass(Constants.ASPECTS.RED);
});

test('Test flashing checkbox', () => {

    // mock the method for state change
    const mockFlashing = jest.fn();

    // render the Light component
    const container = render(<Light initialState="red" flashingChange={mockFlashing} id="test" />);

    // find the flashing checkbox
    const checkbox = container.getByLabelText("Flashing");

    // click the checkbox
    fireEvent.click(checkbox);

    // verify that the mock method has been called
    expect(mockFlashing).toHaveBeenCalledWith(true);

    // click the checkbox again
    fireEvent.click(checkbox);

    // verify that the mock method has been called again.
    expect(mockFlashing).toHaveBeenLastCalledWith(false);
});