import { render, cleanup } from '@testing-library/react';
import Inidcation from "./Indication";
import * as Constants from './constants';

afterEach(cleanup);

test('Test Stop aspect', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.RED} topFlashing={false} middleAspect={Constants.ASPECTS.RED} middleFlashing={false} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Stop");
});

test('Test Approach indication', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.YELLOW} topFlashing={false} middleAspect={Constants.ASPECTS.RED} middleFlashing={false} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Approach");
});

test('Test Clear indication', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.GREEN} topFlashing={false} middleAspect={Constants.ASPECTS.RED} middleFlashing={false} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Clear");
});

test('Test Restricting indication 1', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.LUNAR} topFlashing={false} middleAspect={Constants.ASPECTS.RED} middleFlashing={false} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Restricting");
});

test('Test Restricting indication 2', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.RED} topFlashing={false} middleAspect={Constants.ASPECTS.LUNAR} middleFlashing={false} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Restricting");
});

test('Test Restricting indication 3', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.RED} topFlashing={false} middleAspect={Constants.ASPECTS.RED} middleFlashing={false} bottomAspect={Constants.ASPECTS.LUNAR} bottomFlashing={false}/>);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Restricting");
});

test('Test Restricting indication 4', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.RED} topFlashing={true} middleAspect={Constants.ASPECTS.RED} middleFlashing={false} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Restricting");
});

test('Test Restricting indication 5', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.RED} topFlashing={false} middleAspect={Constants.ASPECTS.RED} middleFlashing={true} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Restricting");
});

test('Test Restricting indication 6', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.RED} topFlashing={false} middleAspect={Constants.ASPECTS.RED} middleFlashing={false} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={true} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Restricting");
});

test('Test Advance Approach 1', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.YELLOW} topFlashing={false} middleAspect={Constants.ASPECTS.YELLOW} middleFlashing={false} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Advance Approach");
});

test('Test Advance Approach 2', () => {
    // By pattern, this should be Approach Medium, but that would be fail deadly for the relay that flashes the top light
    const container = render(<Inidcation topAspect={Constants.ASPECTS.YELLOW} topFlashing={true} middleAspect={Constants.ASPECTS.RED} middleFlashing={false} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Advance Approach");
});

test('Test Approach Limited', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.YELLOW} topFlashing={false} middleAspect={Constants.ASPECTS.GREEN} middleFlashing={true} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Approach Limited");
});

test('Test Limited Clear', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.RED} topFlashing={false} middleAspect={Constants.ASPECTS.GREEN} middleFlashing={true} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Limited Clear");
});

test('Test Approach Medium', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.YELLOW} topFlashing={false} middleAspect={Constants.ASPECTS.GREEN} middleFlashing={false} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Approach Medium");
});

test('Test Medium Clear', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.RED} topFlashing={false} middleAspect={Constants.ASPECTS.GREEN} middleFlashing={false} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Medium Clear");
});

test('Test Approach Slow', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.YELLOW} topFlashing={false} middleAspect={Constants.ASPECTS.RED} middleFlashing={false} bottomAspect={Constants.ASPECTS.GREEN} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Approach Slow");
});

test('Test Slow Clear', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.RED} topFlashing={false} middleAspect={Constants.ASPECTS.RED} middleFlashing={false} bottomAspect={Constants.ASPECTS.GREEN} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Slow Clear");
});

test('Test Approach Restricted', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.YELLOW} topFlashing={false} middleAspect={Constants.ASPECTS.LUNAR} middleFlashing={false} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Approach Restricting");
});

test('Test Limited Approach', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.RED} topFlashing={false} middleAspect={Constants.ASPECTS.YELLOW} middleFlashing={true} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Limited Approach");
});

test('Test Medium Approach', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.RED} topFlashing={false} middleAspect={Constants.ASPECTS.YELLOW} middleFlashing={false} bottomAspect={Constants.ASPECTS.RED} bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Medium Approach");
});

test('Test Slow Approach', () => {
    const container = render(<Inidcation topAspect={Constants.ASPECTS.RED} topFlashing={false} middleAspect={Constants.ASPECTS.RED} middleFlashing={false} bottomAspect={Constants.ASPECTS.YELLOW}bottomFlashing={false} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("Slow Approach");
});

/*
test('', () => {
    const container = render(<Inidcation topAspect={} topFlashing={} middleAspect={} middleFlashing={} bottomAspect={} bottomFlashing={} />);
    const indication = container.getByTestId("indication-name");
    expect(indication.innerHTML).toEqual("");
});
*/