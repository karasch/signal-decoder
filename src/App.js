import React, {useState} from 'react';
import './App.css';
import Light from './Light';
import Indication from './Indication';
import * as Constants from './constants'

const  App = () => {

  const [topLight, setTopLight] = useState(Constants.ASPECTS.RED);
  const [midLight, setMidLight] = useState(Constants.ASPECTS.RED);
  const [botLight, setBotLight] = useState(Constants.ASPECTS.RED);

  const [topFlash, setTopFlash] = useState(false);
  const [midFlash, setMidFlash] = useState(false);
  const [botFlash, setBotFlash] = useState(false);

  console.log("TOP: " + topLight + " " + (topFlash ? "Flashing" : "Steady"));
  console.log("MIDDLE: " + midLight + " "  + (midFlash ? "Flashing" : "Steady"));
  console.log("BOTTOM: " + botLight + " " + (botFlash ? "Flashing" : "Steady"));

  return (
    <div className="wholething">
      <div className="header">
        <h1>North American Railroad Signal Decoder</h1>
        <p>
          Click on the lights below to change their colors, and the name and meaning of the signal will be displayed.
        </p>
      </div>
      <div className="App">

        <div className="signalpost">
          <Light initialState={Constants.ASPECTS.RED} colorChange={setTopLight} flashingChange={setTopFlash} key={1} id="top" />
          <Light initialState={Constants.ASPECTS.RED} colorChange={setMidLight} flashingChange={setMidFlash} key={2} id="mid" />
          <Light initialState={Constants.ASPECTS.RED} colorChange={setBotLight} flashingChange={setBotFlash} key={3} id="bot" />
        </div>
        <Indication topAspect={topLight} topFlashing={topFlash} middleAspect={midLight} middleFlashing={midFlash} bottomAspect={botLight} bottomFlashing={botFlash} />
      </div>
      <div className="footer">
        <p>
          Your railroad and/or signals may be different.  This site is not a replacement for your railroad's operating rulebook.
        </p>
        <p>
          If there is no light in a position on a signal, that is the same as a red light being in that position.
        </p>
        <h2>Speed Limits</h2>
        <ul>
          <li><b>Track Speed</b> is the speed a train is allowed to go if no signal slows it down.  Speed limits are related to track layout and are posted separately from signals.</li>
          <li><b>Limited Speed</b> is generally 45 mph.</li>
          <li><b>Medium Speed</b> is generally 30 mph.</li>
          <li><b>Slow Speed</b> is generally 15 mph.</li>
          <li><b>Restricted Speed</b> is slow enough that a train can stop within half of visual range. Max restricted speed is generally 15 mph.</li>
        </ul>
      </div>
    </div>
  );
}

export default App;
