export const ASPECTS = {
    RED: 'red',
    YELLOW: 'yellow',
    GREEN: 'green',
    LUNAR: 'lunar'
};