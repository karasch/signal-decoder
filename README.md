# Signal Decoder

A React app to translate North American railroad signals into their name and meaning.

The app displays three HTML representations of searchlight-style lights, which change color when clicked.  Each light has a "Flashing" checkbox to represent that light flashing.

This application has been deployed to http://michaelkarasch.com/signals/

## Caveat

This app has my best understanding of railroad signals.  It should not be confused with an actual FRA or railroad rulebook.

## Other Resources

This app was created with information from
* http://www.mikeroque.com/railroad-signals/
* https://signals.jovet.net/rules/CSX%20Signal%20Rules.pdf
* https://www.csx.com/index.cfm/about-us/company-overview/railroad-dictionary/

## Development Stuff

This app was started with Create React App.

All the usual stuff should work:
* `yarn start`
* `yarn test`
* `yarn build`
* `yarn eject`

For more information see:
* [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).
* [React documentation](https://reactjs.org/).

### TODO List
* Medium Approach Medium indication
* Medium Approach Slow indication
* Medium Advance Approach indication.
* Add a few "no such signal" Indication test cases.
* Add more test cases at the App level.
* "No light in this position" checkboxes
